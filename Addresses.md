A reference list of offsets/addresses of relevant code.



## unreal related

### globals

| name          | offset       | description                    | type               |
| ------------- | ------------ | ------------------------------ | ------------------ |
| GlobalObjects | `0x01523220` | global array of all `UObject`s | `TArray<UObject*>` |
| GlobalNames   | `0x015231F0` | global array of all `GName`s   | `TArray<FName>`    |
| ProcessEvent  | `0x61530`    | game update handler            |                    |


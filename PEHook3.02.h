#include "windows.h"
#include <iostream>
#include <fstream>

/* 	===================
	Addresses & Offsets
	=================== */

int BaseModule = (int)::GetModuleHandle(NULL);

#define 	ProcessEvent		(BaseModule + 0x61530)
#define		ProcessEventIndex	67
#define		ProcessEventHook	(BaseModule + 0x61586)
#define		ProcessEventReturn	(BaseModule + 0x6158D)

/* 	===================
	  Helper Functions
	=================== */

void MakeJMP(BYTE *pAddress, DWORD dwJumpTo, DWORD dwLen)
{
	DWORD dwOldProtect, dwBkup, dwRelAddr;
	VirtualProtect(pAddress, dwLen, PAGE_EXECUTE_READWRITE, &dwOldProtect);
	dwRelAddr = (DWORD)(dwJumpTo - (DWORD)pAddress) - 5;
	*pAddress = 0xE9;
	*((DWORD *)(pAddress + 0x1)) = dwRelAddr;
	for (DWORD x = 0x5; x < dwLen; x++) *(pAddress + x) = 0x90;
	VirtualProtect(pAddress, dwLen, dwOldProtect, &dwBkup);
	return;
}

/* 	===================
	   Process Event
	=================== */

DWORD pCallerObject = NULL;
DWORD pFunction = NULL;
DWORD pParams = NULL;

void __declspec(naked) hkProcessEvent()
{
		__asm mov pCallerObject, ecx;
	__asm
	{
		PUSH	EAX
		MOV		EAX, DWORD PTR[ESP + 0x8]
		MOV		pFunction, EAX
		MOV		EAX, DWORD PTR[ESP + 0xC]
		MOV		pParams, EAX
		POP		EAX
	}

	// do stuff

	__asm
	{
		XOR		EBX, [ECX + EAX * 4]
		JMP		[dwProcessEventReturn]
	}
}

/* 	==========================
	  Create Thread & Hook PE
	========================== */

void OnAttach()
{
	MakeJMP((BYTE*)GProcessEvent, (DWORD)hkProcessEvent, 0x5);
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)OnAttach, NULL, NULL, NULL);
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
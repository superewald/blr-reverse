A collection of tools and notes for debugging and reversing BL:R.

1. [Ghidra](Ghidra/)
2. [IDA](FoxGame-win32-Shiping.i64)
3. [Network Captures](ReCaps/)
4. [Classes (ReClass.NET)](ReClass/)
5. [Offsets/Addresses](Addresses.md)
6. [Snippets]()